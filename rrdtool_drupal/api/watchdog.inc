<?php
// $id: $

function rrdtool_drupal_watchdog_rrdcreate() {
  return array('watchdog' => array(
    'action' => NULL,
    'access denied' => NULL,
    'aggregator' => NULL,
    'content' => NULL,
    'cron' => NULL,
    'error' => NULL,
    'field' => NULL,
    'file' => NULL,
    'file system' => NULL,
    'image' => NULL,
    'locale' => NULL,
    'menu' => NULL,
    'node' => NULL,
    'page not found' => NULL,
    'php' => NULL,
    'profile' => NULL,
    'search' => NULL,
    'system' => NULL,
    'security' => NULL,
    'taxonomy' => NULL,
    'tracker' => NULL,
    'user' => NULL,
    'unknown' => NULL,
  ));
}

function rrdtool_drupal_watchdog_rrdgraph() {
  return array('watchdog' => array(
    'content' => array(
      '--start', '%seconds',
      '-Y', '--title=Content logs',
      'DEF:node=%rrdfile:node:AVERAGE',
      'DEF:content=%rrdfile:content:AVERAGE',
      'DEF:taxonomy=%rrdfile:taxonomy:AVERAGE',
      'DEF:user=%rrdfile:user:AVERAGE',
      'DEF:profile=%rrdfile:profile:AVERAGE',
      'DEF:tracker=%rrdfile:tracker:AVERAGE',
      'LINE1:node#00FF00:node',
      'LINE1:content#0000FF:content',
      'LINE1:taxonomy#FF0000:taxonomy\\r',
      'LINE1:user#FF00FF:user',
      'LINE1:profile#00FFFF:profile',
      'LINE1:tracker#FFFF00:tracker\\r',
    ),
    'error' => array(
      '--start', '%seconds',
      '-Y', '--title=Error logs',
      'DEF:page_not_found=%rrdfile:page_not_found:AVERAGE',
      'DEF:access_denied=%rrdfile:access_denied:AVERAGE',
      'DEF:php=%rrdfile:php:AVERAGE',
      'DEF:unknown=%rrdfile:unknown:AVERAGE',
      'LINE1:page_not_found#00FF00:page not found',
      'LINE1:access_denied#0000FF:access denied',
      'LINE1:php#FF0000:php',
      'LINE1:unknown#FF00FF:unknown\\r',
    ),
    'system' => array(
      '--start', '%seconds',
      '-Y', '--title=System logs',
      'DEF:system=%rrdfile:system:AVERAGE',
      'DEF:cron=%rrdfile:cron:AVERAGE',
      'DEF:menu=%rrdfile:menu:AVERAGE',
      'DEF:action=%rrdfile:action:AVERAGE',
      'LINE1:system#00FF00:system',
      'LINE1:cron#0000FF:cron',
      'LINE1:menu#FF0000:menu',
      'LINE1:action#FF00FF:action\\r',
    ),
    'other' => array(
      '--start', '%seconds',
      '-Y', '--title=Other logs',
      'DEF:field=%rrdfile:field:AVERAGE',
      'DEF:file=%rrdfile:file:AVERAGE',
      'DEF:file_system=%rrdfile:file_system:AVERAGE',
      'DEF:image=%rrdfile:image:AVERAGE',
      'DEF:locale=%rrdfile:locale:AVERAGE',
      'DEF:security=%rrdfile:security:AVERAGE',
      'DEF:search=%rrdfile:search:AVERAGE',
      'LINE1:field#00FF00:field',
      'LINE1:file#0000FF:file',
      'LINE1:file_system#FF0000:file system\\r',
      'LINE1:image#FF00FF:image',
      'LINE1:locale#00FFFF:locale',
      'LINE1:security#FFFF00:security',
      'LINE1:search#DADA00:search\\r',
    ),
  ));
}

/**
 * Log statistics from core watchdog.
 */
function rrdtool_drupal_watchdog(array $log_entry) {
  $rrds = rrdtool_drupal_rrdcreate();
  $key = (array_key_exists($log_entry['type'], $rrds['watchdog'])) ? $log_entry['type'] : 'unknown';
  rrdtool_monitors_increment('watchdog', $key);
}

