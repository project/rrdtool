<?php

function rrdtool_drupal_bootstrap_rrdcreate() {
  return array('bootstrap' => array(
    'boot' => NULL,
    'init' => NULL,
    'exit' => NULL,
  ));
}

function rrdtool_drupal_bootstrap_rrdgraph() {
  return array('bootstrap' => array(
    'bootstrap' => array(
      '--start', '%seconds',
      '-Y', '--title=Bootstrap',
      'DEF:boot=%rrdfile:boot:AVERAGE',
      'DEF:init=%rrdfile:init:AVERAGE',
      'DEF:exit=%rrdfile:exit:AVERAGE',
      'AREA:init#00FF00:hook_init',
      'LINE1:boot#0000FF:hook_boot',
      'LINE1:exit#FF0000:hook_exit\\r',
    ),
  ));
}

function rrdtool_drupal_exit() {
  rrdtool_monitors_increment('bootstrap', 'exit');
}

