<?php

function rrdtool_drupal_userapi_rrdcreate() {
  return array('userapi' => array(
    'load' => NULL,
    'cancel' => NULL,
    'operations' => NULL,
    'categories' => NULL,
    'presave' => NULL,
    'insert' => NULL,
    'update' => NULL,
    'login' => NULL,
    'logout' => NULL,
    'view' => NULL,
    'view_alter' => NULL,
    'role_insert' => NULL,
    'role_update' => NULL,
    'role_delete' => NULL,
  ));
}

function rrdtool_drupal_userapi_rrdgraph() {
  return array('userapi' => array(
    'view' => array(
      '--start', '%seconds',
      '-Y', '--title=Viewing users',
      'DEF:load=%rrdfile:load:AVERAGE',
      'DEF:view=%rrdfile:view:AVERAGE',
      'AREA:load#00FF00:user_load',
      'LINE1:view#0000FF:user_view\\r',
    ),
    'activity' => array(
      '--start', '%seconds',
      '-Y', '--title=User activity',
      'DEF:login=%rrdfile:login:AVERAGE',
      'DEF:logout=%rrdfile:logout:AVERAGE',
      'AREA:login#00FF00:user_login',
      'LINE1:logout#0000FF:user_logout\\r',
    ),
    'edit' => array(
      '--start', '%seconds',
      '-Y', '--title=Editing users',
      'DEF:insert=%rrdfile:insert:AVERAGE',
      'DEF:update=%rrdfile:update:AVERAGE',
      'DEF:cancel=%rrdfile:cancel:AVERAGE',
      'LINE1:insert#00FF00:user_insert',
      'LINE1:update#0000FF:user_update',
      'LINE1:cancel#0000FF:user_cancel\\r',
    ),
    'roles' => array(
      '--start', '%seconds',
      '-Y', '--title=Editing roles',
      'DEF:role_insert=%rrdfile:role_insert:AVERAGE',
      'DEF:role_update=%rrdfile:role_update:AVERAGE',
      'DEF:role_delete=%rrdfile:role_delete:AVERAGE',
      'LINE1:role_insert#00FF00:user_role_insert',
      'LINE1:role_update#0000FF:user_role_update',
      'LINE1:role_delete#0000FF:user_role_delete\\r',
    ),
  ));
}

/**
 * Hook into userapi.
 */
function rrdtool_user_load() {
  rrdtool_monitors_increment('userapi', 'load');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_cancel() {
  rrdtool_monitors_increment('userapi', 'cancel');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_operations() {
  rrdtool_monitors_increment('userapi', 'operations');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_categories() {
  rrdtool_monitors_increment('userapi', 'categories');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_presave() {
  rrdtool_monitors_increment('userapi', 'presave');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_insert() {
  rrdtool_monitors_increment('userapi', 'insert');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_update() {
  rrdtool_monitors_increment('userapi', 'update');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_login() {
  rrdtool_monitors_increment('userapi', 'login');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_logout() {
  rrdtool_monitors_increment('userapi', 'logout');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_view() {
  rrdtool_monitors_increment('userapi', 'view');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_view_alter() {
  rrdtool_monitors_increment('userapi', 'view_alter');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_role_insert() {
  rrdtool_monitors_increment('userapi', 'role_insert');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_role_update() {
  rrdtool_monitors_increment('userapi', 'role_update');
}

/**
 * Hook into userapi.
 */
function rrdtool_user_role_delete() {
  rrdtool_monitors_increment('userapi', 'role_delete');
}

