<?php

function rrdtool_drupal_nodeapi_rrdcreate() {
  return array('nodeapi' => array(
    'grants' => NULL,
    'access_records' => NULL,
    'delete' => NULL,
    'revision_delete' => NULL,
    'insert' => NULL,
    'load' => NULL,
    'access' => NULL,
    'prepare' => NULL,
    'prepare_translation' => NULL,
    'search_result' => NULL,
    'presave' => NULL,
    'update' => NULL,
    'update_index' => NULL,
    'validate' => NULL,
    'view' => NULL,
    'view_alter' => NULL,
    'info' => NULL,
    'type_insert' => NULL,
    'type_update' => NULL,
    'type_delete' => NULL,
    'ranking' => NULL,
    'form' => NULL,
  ));
}

function rrdtool_drupal_nodeapi_rrdgraph() {
  return array('nodeapi' => array(
    'view' => array(
      '--start', '%seconds',
      '-Y', '--title=Viewing nodes',
      'DEF:view=%rrdfile:view:AVERAGE',
      'DEF:load=%rrdfile:load:AVERAGE',
      'AREA:load#00FF00:node_load',
      'LINE1:view#0000FF:node_view\\r',
    ),
    'edit' => array(
      '--start', '%seconds',
      '-Y', '--title=Editing nodes',
      'DEF:insert=%rrdfile:insert:AVERAGE',
      'DEF:update=%rrdfile:update:AVERAGE',
      'DEF:delete=%rrdfile:delete:AVERAGE',
      'LINE1:insert#00FF00:node_insert',
      'LINE1:update#0000FF:node_update',
      'AREA:delete#FF0000:node_delete\\r',
    ),
  ));
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_grants() {
  rrdtool_monitors_increment('nodeapi', 'grants');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_access_records() {
  rrdtool_monitors_increment('nodeapi', 'access_records');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_delete() {
  rrdtool_monitors_increment('nodeapi', 'delete');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_revision_delete() {
  rrdtool_monitors_increment('nodeapi', 'revision_delete');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_insert() {
  rrdtool_monitors_increment('nodeapi', 'insert');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_load() {
  rrdtool_monitors_increment('nodeapi', 'load');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_access() {
  rrdtool_monitors_increment('nodeapi', 'access');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_prepare() {
  rrdtool_monitors_increment('nodeapi', 'prepare');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_prepare_translation() {
  rrdtool_monitors_increment('nodeapi', 'prepare_translation');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_search_result() {
  rrdtool_monitors_increment('nodeapi', 'search_result');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_presave() {
  rrdtool_monitors_increment('nodeapi', 'presave');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_update() {
  rrdtool_monitors_increment('nodeapi', 'update');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_update_index() {
  rrdtool_monitors_increment('nodeapi', 'update_index');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_validate() {
  rrdtool_monitors_increment('nodeapi', 'validate');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_view() {
  rrdtool_monitors_increment('nodeapi', 'view');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_view_alter() {
  rrdtool_monitors_increment('nodeapi', 'view_alter');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_info() {
  rrdtool_monitors_increment('nodeapi', 'info');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_type_insert() {
  rrdtool_monitors_increment('nodeapi', 'type_insert');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_type_update() {
  rrdtool_monitors_increment('nodeapi', 'type_update');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_type_delete() {
  rrdtool_monitors_increment('nodeapi', 'type_delete');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_drupal_node_ranking() {
  rrdtool_monitors_increment('nodeapi', 'ranking');
}

/**
 * Hook into nodeapi.
 */
function rrdtool_form() {
  rrdtool_monitors_increment('nodeapi', 'form');
}

