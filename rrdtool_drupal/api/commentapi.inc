<?php

function rrdtool_drupal_commentapi_rrdcreate() {
  return array('commentapi' => array(
    'presave' => NULL,
    'insert' => NULL,
    'update' => NULL,
    'load' => NULL,
    'view' => NULL,
    'publish' => NULL,
    'unpublish' => NULL,
    'delete' => NULL,
  ));
}

function rrdtool_drupal_commentapi_rrdgraph() {
  return array('commentapi' => array(
    'view' => array(
      '--start', '%seconds',
      '-Y', '--title=Viewing comments',
      'DEF:view=%rrdfile:view:AVERAGE',
      'DEF:load=%rrdfile:load:AVERAGE',
      'AREA:load#00FF00:comment_load',
      'LINE1:view#0000FF:comment_view\\r',
    ),
    'edit' => array(
      '--start', '%seconds',
      '-Y', '--title=Editing comments',
      'DEF:insert=%rrdfile:insert:AVERAGE',
      'DEF:update=%rrdfile:update:AVERAGE',
      'DEF:delete=%rrdfile:delete:AVERAGE',
      'AREA:insert#00FF00:comment_insert',
      'LINE1:update#FF0000:comment_update',
      'LINE1:delete#0000FF:comment_delete\\r',
    ),
    'publish' => array(
      '--start', '%seconds',
      '-Y', '--title=Publishing comments',
      'DEF:publish=%rrdfile:publish:AVERAGE',
      'DEF:unpublish=%rrdfile:unpublish:AVERAGE',
      'AREA:publish#00FF00:comment_publish',
      'LINE1:unpublish#0000FF:comment_unpublish\\r',
    ),
  ));
}

/**
 * Hook into commentapi.
 */
function rrdtool_drupal_comment_presave() {
  rrdtool_monitors_increment('commentapi', 'presave');
}

/**
 * Hook into commentapi.
 */
function rrdtool_drupal_comment_insert() {
  rrdtool_monitors_increment('commentapi', 'insert');
}

/**
 * Hook into commentapi.
 */
function rrdtool_drupal_comment_update() {
  rrdtool_monitors_increment('commentapi', 'update');
}

/**
 * Hook into commentapi.
 */
function rrdtool_drupal_comment_load() {
  rrdtool_monitors_increment('commentapi', 'load');
}

/**
 * Hook into commentapi.
 */
function rrdtool_drupal_comment_view() {
  rrdtool_monitors_increment('commentapi', 'view');
}

/**
 * Hook into commentapi.
 */
function rrdtool_drupal_comment_publish() {
  rrdtool_monitors_increment('commentapi', 'publish');
}

/**
 * Hook into commentapi.
 */
function rrdtool_drupal_comment_unpublish() {
  rrdtool_monitors_increment('commentapi', 'unpublish');
}

/**
 * Hook into commentapi.
 */
function rrdtool_drupal_comment_delete() {
  rrdtool_monitors_increment('commentapi', 'delete');
}

