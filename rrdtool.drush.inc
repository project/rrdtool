<?php
// $Id $

/**
 * @file
 *   rrdtool drush integration
 */

/**
 * Implementation of hook_drush_command().
 */
function rrdtool_drush_command() {
  $items = array();

  $items['rrdtool-sync'] = array(
    'description' => "Sync counters into rrd.",
    'options' => array(
      'loop' => "How many times to sync data into rrd.  Defaults to 240.",
      'delay' => "How long to delay between each time we loop.  Defaults to 15.",
    )
  );

  return $items;
}

function rrdtool_drush_help() {
  switch ($section) {
    case 'drush:rrdtool-sync':
      return dt('Pulls data out of memcache and updates the watchdog rrd.');
  }
}

function drush_rrdtool_sync() {
  $loop = drush_get_option('loop');
  if (empty($loop)) {
    $loop = 240;
  }
  $delay = drush_get_option('delay');
  if (empty($delay)) {
    $delay = 15;
  }

  for ($i = 0; $i < $loop; $i++) {
    sleep ($delay);
    print_r(rrdtool_sync()) ."\n";
  }
}
