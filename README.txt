Overview:
---------
These modules provide apis for integrating drupal with RRDtool, the round-robin
database tool, used for tracking time-series data.  The rrdtool module provides
apis for writing data to and generating graphs from round-robin databases.  The
rrdtool_monitors module provides apis for incrementing counters in a persistent,
distributed data store (currently only memcache is supported).  And the
rrdtool_drupal module tracks statistics from Drupal's core apis.

All three modules can run on the same web server, or the rrdtool_monitors and
rrdtool_drupal modules can be enabled on multiple production webservers with
the rrdtool module enabled only on a private monitoring server.

Requirements:
-------------
 - memcache
   The rrdtool and rrdtool_monitors modules utilize memcache for storing
   counters in a distributed and reasonably persistant way.
     http://memcached.org/
     http://pecl.php.net/package/memcache

 - rrdtool php extension
   The rrdtool module utilizes the rrdtool php extension to write to
   and read from round-robin database files, and to generate images.  It is
   not required for you to install the rrdtool php extension on your production
   web servers.
     http://oss.oetiker.ch/rrdtool

Installation:
-------------
Follow the directions at http://drupal.org/project/memcache to get memcache
properly installed and configured.  The memcache drupal module is _not_
required in any way, however installation and configuration is exactly as is
described there.

If available, install the php-rrdtool package provided by your operating
system.  If a package is not provided, you can install the extension from
source.  Unfortunately, however, the rrdtool php extension is not distributed
as a pecl-package.

The following detail how to install the php-rrdtool package from source on
a CentOS system.  Installation on another distribution should be similar.

1) Download, compile and install the latest stable rrdtool package from
http://oss.oetiker.ch/rrdtool/pub/

The latest package can be downloaded from this URL:
http://oss.oetiker.ch/rrdtool/pub/rrdtool.tar.gz

It is likely that when you run ./configure the process will end with errors.
Some rpms that you will need to be sure are installed include:
 - cairo-devel, pango-devel, glib2-devel

By default, rrdtool will be installed in /opt.

2) Next, download, compile and install the php-rrdtool extension from
http://oss.oetiker.ch/rrdtool/pub/contrib/

The latest package can be downloaded from this URL:
http://oss.oetiker.ch/rrdtool/pub/contrib/php_rrdtool.tar.gz

Extract this tarball into your php extension include directory, which should be
at /usr/local/include/php/ext.

Cd into that directory and run 'phpize' then run './configure'.  You will need
to specify the "--with-rrdtool=" option and point it to the rrdtool that you
just compiled.  For example:
  ./configure --with-rrdtool=/opt/rrdtool-1.4.2

Run 'make && make install' to compile and install this php extension.

3) Enable the rrdtool extension in php.ini, or php.d/rrdtool.ini and adding
the following line:
  extension=rrdtool.so

4) Restart apache and review the output from phpinfo() to confirm that
the rrdtool extension is properly loaded.  At the same time, be sure that the
memcache PHP extension is also properly loaded.

5) Enable the rrdtool.module.
